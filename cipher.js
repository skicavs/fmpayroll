// This script takes user input and returns it encrypted and in plain text
// Originally developed: April 21, 1997.  Updated: February 2002.

// Validate the input
function validateInput (form) { 
	if (document.CipherForm.strString.value == "") { 
	   alert("Please enter something to encrypt."); 
	   document.CipherForm.strString.focus(); 
	   return false; 
	}
}

function flipLetter (chrLetter) { 
	//  Executes a reflection cipher on the character passed from encryptString()   
	//  substituting one letter for its alphabetic inverse
	switch(chrLetter) {
		case "a": chrLetter = "z"; break;
		case "b": chrLetter = "y"; break;
		case "c": chrLetter = "x"; break;
		case "d": chrLetter = "w"; break;
		case "e": chrLetter = "v"; break;
		case "f": chrLetter = "u"; break;
		case "g": chrLetter = "t"; break;
		case "h": chrLetter = "s"; break;
		case "i": chrLetter = "r"; break;
		case "j": chrLetter = "q"; break;
		case "k": chrLetter = "p"; break;
		case "l": chrLetter = "o"; break;
		case "m": chrLetter = "n"; break;
		case "n": chrLetter = "m"; break;
		case "o": chrLetter = "l"; break;
		case "p": chrLetter = "k"; break;
		case "q": chrLetter = "j"; break;
		case "r": chrLetter = "i"; break;
		case "s": chrLetter = "h"; break;
		case "t": chrLetter = "g"; break;
		case "u": chrLetter = "f"; break;
		case "v": chrLetter = "e"; break;
		case "w": chrLetter = "d"; break;
		case "x": chrLetter = "c"; break;
		case "y": chrLetter = "b"; break;
		case "z": chrLetter = "a"; break;
	}
	return chrLetter;
}

function encryptString (strPlain) {
	// Convert the string passed to this function from the form to lower case
	strPlain = strPlain.toLowerCase();
	document.CipherForm.strPlain.value = strPlain;
	
	// Run the reflection cipher on each letter in the string
	var strCipher = "";
	for (var i = 0; i < strPlain.length; i++) { 
		strCipher = strCipher + flipLetter(strPlain.charAt(i));
    }
   
	// Set the input value to the encrypted string
	document.CipherForm.strCipher.value = strCipher;
	document.CipherForm.strCipher.focus();
}